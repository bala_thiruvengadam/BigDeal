function pageprocessorMaps(pagedef, $scope, $routeParams, $compile, $http, $rootScope, $sce, $window, $location) {
  var data1 = $compile($(pagedef.pageTemplate))($scope);
  $("#appContent")
    .append(data1);



  function enableOrientationArrow() {

    if (window.DeviceOrientationEvent) {

      window.addEventListener('deviceorientation', function(event) {
        var alpha = null;
        //Check for iOS property
        if (event.webkitCompassHeading) {
          alpha = event.webkitCompassHeading;
        }
        //non iOS
        else {
          alpha = event.alpha;
        }
        var locationIcon = myLocationMarker.get('icon');
        locationIcon.rotation = 360 - alpha;
        myLocationMarker.set('icon', locationIcon);
      }, false);
    }
  }


  $scope.loaddata = function() {

    $scope.selitem = "";
    $scope.markers = [];
    var points = $.parseJSON(pagedef.mapmarkers);

    var items = [];
    $.each(points, function(key, val) {
      var item = {
        "name": val.desc,
        "clat": val.lat,
        "clong": val.long,
        "swlat": val.swLat,
        "swlong": val.swLong,
        "nelat": val.neLat,
        "nelong": val.neLong,
        "zoom": "17",
        "markers": [{
          "lat": val.lat,
          "long": val.long,
          "desc": val.desc
        }],
        "floor": val.floor
      };
      items.push(item);
    });
    items.sort(function(a, b) {
      if (a.name < b.name)
        return -1;
      if (a.name > b.name)
        return 1;
      return 0;
    });
    $scope.items = items;

    $rootScope.items = items;
    $scope.$apply();
    $.unblockUI();
  }


  function initMap() {
    var myOptions = {
      zoom: 17,
      mapTypeId: google.maps.MapTypeId.SATELLITE
    };
    var mapDisplay = document.getElementById("MapMapCanvas")
    var mapheight = $(window).height() - ($("#appContent .list-group-item").height() + $(".navbar-absolute-top").height() + $(".navbar-absolute-bottom").height() + 20)
    $("#MapMapCanvas").css("height", mapheight + "px");
    var map = new google.maps.Map(mapDisplay, myOptions);


    map.setTilt(45);
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {

        var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

        var infowindow = new google.maps.InfoWindow({
          map: map,
          position: pos,
          content: 'You are here'
        });
        map.setCenter(pos);
      }, function() {
        //handleNoGeolocation(true);
        navigator.notification.alert("Unable to find your location. Please check your GPS settings, network connectivity and Location services.", function() {
          window.location.href = "index.html#home";
          $.unblockUI();
        }, "Location services disabled", "OK");
      }, {
        timeout: 10000,
        frequency: 3000,
        enableHighAccuracy: true
      });
    }
  }

  var cb = function() {
    var geocoder;
    var placeCoordinate;
    var errorCallback = function(error) {
      $.unblockUI();
      navigator.notification.alert("Unable to find your location. Please check your GPS settings, network connectivity and Location services.", function() {}, 'Alert', 'Ok');
      $scope.trackinginfo = "Unable to find your location. Please check your GPS settings, network connectivity and Location services." + error.message + " ( " + error.code + " )";
      $scope.$digest();
    }
    var placeGeoCoder = function(results) {
      try {
        $.unblockUI();
        $scope.breakOut = false;
        $rootScope.breakOut = false;
        var myLat = results.coords.latitude;
        var myLong = results.coords.longitude;
        //var myLat = 40.60967808904413;
        //var myLong = -74.68763452950604;
        $scope.currentLat = myLat;
        $scope.currentLong = myLong;
        //navigator.notification.alert(myLat + ' , ' + myLong, function() {}, 'Your Current Location', 'Dismiss');

        //MAP
        var mapOptions = {
          center: new google.maps.LatLng(myLat, myLong),
          zoom: 17,
          mapTypeId: google.maps.MapTypeId.SATELLITE
        };

        var map = new google.maps.Map(document.getElementById("MapMapCanvas"),
          mapOptions);

        //var image = "http://www.dreamrealty.com/images/blue_dot_circle.png";
        var image = "http://rottenwifi.com/bundles/itowififront/images/my-location-icon.png";

        //var points = $.parseJSON(pagedef.mapmarkers);
        /*$.each(points, function(key, val) {
             var sw = new google.maps.LatLng(val.swLat, val.swLong);
             var ne = new google.maps.LatLng(val.neLat, val.neLong);
             var bounds = new google.maps.LatLngBounds(sw, ne);

             if (bounds.contains(new google.maps.LatLng(myLat, myLong))) {
                 alert("Dismiss");
                 $scope.Building = val;
                 $scope.breakOut = true;
                 $rootScope.breakOut = true;
                 return false;
             }

         });*/

        var sw = new google.maps.LatLng(40.607785, -74.693630);
        var ne = new google.maps.LatLng(40.615355, -74.684259);
        var bounds = new google.maps.LatLngBounds(sw, ne);

        if (bounds.contains(new google.maps.LatLng(myLat, myLong))) {

          $scope.Building = val;
          $scope.breakOut = true;
          $rootScope.breakOut = true;
          return false;
        }



        if ($scope.breakOut) {

          /*breakOut = false;
          return false;*/
          var marker = new google.maps.Marker({
            position: new google.maps.LatLng(myLat, myLong),
            map: map,
            title: "You are here",
            icon: image
              /*icon: {
                  path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
                  strokeColor: '#3333FF',
                  strokeWeight: 5,
                  scale: 2.5
              },*/
          });
          //enableWatchPosition();
          enableOrientationArrow();
          var imageBounds = new google.maps.LatLngBounds(
            new google.maps.LatLng($scope.Building.swLat, $scope.Building.swLong),
            new google.maps.LatLng($scope.Building.neLat, $scope.Building.neLong));

          historicalOverlay = new google.maps.GroundOverlay(
            'https://www.dropbox.com/s/pujmjyahw3e1itk/RVCC_campusmap.jpg?dl=1',
            imageBounds);
          historicalOverlay.setMap(map);

        } else {
          initMap();
        }



      } catch (e) {
        $.unblockUI();
        //alert("Error : " + e);
      }
    }

    $scope.placeGeoCoder = placeGeoCoder;
    $scope.errorCallback = errorCallback;
    if (navigator.geolocation) {
      $.blockUI();
      navigator.geolocation.getCurrentPosition(placeGeoCoder, errorCallback, {
        timeout: 10000,
        frequency: 3000,
        enableHighAccuracy: true
      });
    } else {
      //alert("not working")
    }
    /* window.setInterval(function(){
  cb();
}, 5000);*/
  };
  window.cb = cb;
  /*if (typeof google === 'object' && typeof google.maps === 'object') {
      window.cb(true);
  } else {*/
  var script = document.createElement("script");
  script.type = "text/javascript";
  script.src = "https://maps.google.com/maps/api/js?sensor=false&callback=cb";
  document.body.appendChild(script);

  $scope.walkdirection = "btn-primary";
  $scope.cycledirection = "";
  $scope.drivedirection = "";
  $scope.selectitem = function() {

    if ($scope.breakOut) {

      var item = $scope.items[$scope.selitem];
      $rootScope.Building = item;
      $location.path("/app/Maps/MapsPage2")
    } else {

      $("#building_selector").blur();
      var item = $scope.items[$scope.selitem];
      $rootScope.Building = item;
      if (item) {
        $scope.curitem = item;
        try {
          var cb1 = function() {

            var geocoder;
            var placeCoordinate;
            var errorCallback = function(error) {
              $.unblockUI();
              navigator.notification.alert("Unable to locate your location. Please check your GPS settings, network connectivity and Location services.", function() {}, 'Alert', 'Ok');
              $scope.trackinginfo = "Unable to locate your location. Please check your GPS settings, network connectivity and Location services." + error.message + " ( " + error.code + " )";
              $scope.$digest();
            }
            var placeGeoCoder = function(results) {
              try {
                $.unblockUI();


                placeCoordinate = results;
                $rootScope.mylocation = results;
                var latlng = new google.maps.LatLng(item.clat, item.clong);

                var myOptions = {
                  zoom: 17,
                  center: latlng,
                  mapTypeId: google.maps.MapTypeId.SATELLITE
                };
                var mapDisplay = document.getElementById("MapMapCanvas")
                var map = new google.maps.Map(mapDisplay, myOptions);
                var myloc = new google.maps.Marker({
                  position: latlng,
                  map: map,
                  title: "title"
                });
                map.setTilt(45);
                $scope.markers.push(myloc);
                $scope.map = map;
                $scope.walk();
              } catch (e) {
                $.unblockUI();
                navigator.notification.alert("Unknown error occurred");
              }
            }

            $scope.placeGeoCoder = placeGeoCoder;
            $scope.errorCallback = errorCallback;
            if (navigator.geolocation) {
              $.blockUI();
              navigator.geolocation.getCurrentPosition(placeGeoCoder, errorCallback, {
                timeout: 10000,
                frequency: 3000,
                enableHighAccuracy: true
              });
            } else {
              navigator.notification.alert("Unknown error occurred");
            }
          };
          window.cb1 = cb1;
          if (typeof google === 'object' && typeof google.maps === 'object') {
            window.cb1(true);
          } else {
            var script = document.createElement("script");
            script.type = "text/javascript";
            script.src = "https://maps.google.com/maps/api/js?sensor=false&callback=cb";
            document.body.appendChild(script);
            //$.getScript("https://maps.googleapis.com/maps/api/js?key=AIzaSyDUbP_Qxh9wbPOvgW8iH_zuLu8K3fSlv0I&sensor=false&callback=cb");
          }

        } catch (e) {
          navigator.notification.alert("Unknown error occurred");
        }

      }
    }
  };

  $scope.travelmode = "Walk";
  $scope.walk = function() {
    $scope.travelmode = "Walk";
    $scope.curtravelmode = "walk";
    $scope.walkdirection = "btn-primary";
    $scope.cycledirection = "";
    $scope.drivedirection = "";
    $scope.directiondisplay("walk", $scope.map, $rootScope.mylocation.coords.latitude, $rootScope.mylocation.coords.longitude, $scope.curitem.clat, $scope.curitem.clong);
    //$scope.directiondisplay("walk",$scope.map, "34.289408","-83.813368", $scope.curitem.clat, $scope.curitem.clong );

  }

  $scope.bicycle = function() {
    $scope.travelmode = "Bicycle";
    $scope.curtravelmode = "cycle";
    $scope.walkdirection = "";
    $scope.cycledirection = "btn-primary";
    $scope.drivedirection = "";
    $scope.directiondisplay("cycle", $scope.map, $rootScope.mylocation.coords.latitude, $rootScope.mylocation.coords.longitude, $scope.curitem.clat, $scope.curitem.clong);
    //$scope.directiondisplay("cycle",$scope.map, "34.289408","-83.813368", $scope.curitem.clat, $scope.curitem.clong );
  }

  $scope.drive = function() {
    $scope.travelmode = "Drive";
    $scope.curtravelmode = "drive";
    $scope.walkdirection = "";
    $scope.cycledirection = "";
    $scope.drivedirection = "btn-primary";
    $scope.directiondisplay("drive", $scope.map, $rootScope.mylocation.coords.latitude, $rootScope.mylocation.coords.longitude, $scope.curitem.clat, $scope.curitem.clong);
  }


  $scope.directiondisplay = function(mode, map, slat, slng, elat, elng) {
    $scope.trackinginfo = "";
    if ($scope.directionsDisplay) {
      $scope.directionsDisplay.setMap(null);
    }
    /*for (i in $scope.markers) {
        $scope.markers[i].setMap(null);
    }*/
    var directionsDisplay = new google.maps.DirectionsRenderer({
      suppressMarkers: false
    });
    var icons = {
      start: new google.maps.MarkerImage(

        // URL
        'images/start-pin.png',
        // (image width,height)
        new google.maps.Size(32, 32),
        //Point for the Image
        new google.maps.Point(0, 0)
      ),
      end: new google.maps.MarkerImage(
        // URL
        'images/end-pin.png',
        // (image width,height)
        new google.maps.Size(32, 32),
        //Point for the Image
        new google.maps.Point(0, 0)
      )
    };
    directionsDisplay.setMap(null);
    directionsDisplay.setMap(map);
    $scope.directionsDisplay = directionsDisplay;

    try {

      var directionsService = new google.maps.DirectionsService();
      var start = new google.maps.LatLng(slat, slng);
      var end = new google.maps.LatLng(elat, elng);
      var travelModeOption = mode;
      var request = {
        origin: start,
        destination: end,
        travelMode: "",
        unitSystem: google.maps.UnitSystem.METRIC
      };

      switch (travelModeOption) {
        case "walk":
          {
            var usertravelMode = "WALKING";
            request.travelMode = google.maps.TravelMode.WALKING;
            break;
          }
        case "cycle":
          {
            var usertravelMode = "BICYCLING";
            request.travelMode = google.maps.TravelMode.BICYCLING;
            break;
          }
        case "drive":
          {
            var usertravelMode = "DRIVING";
            request.travelMode = google.maps.TravelMode.DRIVING;
            break;
          }
        default:
          {
            var usertravelMode = "WALKING";
            request.travelMode = google.maps.TravelMode.WALKING;
            break;
          }
      }
    } catch (e) {
      alert(e)
    }
    directionsService.route(request, function(result, status) {
      if (status == google.maps.DirectionsStatus.OK) {
        try {
          for (i in $scope.markers) {
            $scope.markers[i].setMap(null);
          }
          var len = result.routes[0].legs[0].distance.text.length - 3;
          var miles = (result.routes[0].legs[0].distance.value) * 0.000621;
          var d = miles.toString();
          $scope.distmiles = d.slice(0, 6);
          $scope.duration = JSON.stringify(result.routes[0].legs[0].duration.text);
        } catch (e) {}
        for (i in $scope.markers) {
          $scope.markers[i].setMap(null);
        }
        directionsDisplay.setDirections(result);
        $scope.$digest();
      } else {
        //navigator.notification.alert("Oops! Your device dont have GPS enabled " + status, function() {}, 'Alert', 'Ok');
        $scope.trackinginfo = "Unable to get GPS data. ( " + status + " )";
        $scope.$digest();
      }
    });
  }


  $scope.loaddata();
}